# kiri-giraffe



## Gérération des données pokémon
Aller sur la page de requête de  [poképédia](https://www.pokepedia.fr/index.php?title=Sp%C3%A9cial:Requ%C3%AAter&#search),
et poser en condition :

    [[Catégorie:Page de Pokémon]] [[Numéro National::+]]

Et en sélection des données à imprimer :

    ?Numéro National
    ?Poids
    ?Taille
    ?Premier type
    ?Second type
    ?Catégorie
    ?Génération du Pokémon

Récupérer les données en CSV.


## Résultats Pokémon
Cf [pokemon.py](pokemon.py).

         Acier: 	0.01142 m2
        Combat: 	0.00629 m2
        Dragon: 	0.01213 m2
           Eau: 	0.00513 m2
           Feu: 	0.00532 m2
           Fée: 	0.00235 m2
         Glace: 	0.00815 m2
       Insecte: 	0.00338 m2
        Normal: 	0.00414 m2
        Plante: 	0.00329 m2
        Poison: 	0.00448 m2
           Psy: 	0.00489 m2
         Roche: 	0.00858 m2
           Sol: 	0.00865 m2
       Spectre: 	0.00475 m2
      Ténèbres: 	0.00555 m2
           Vol: 	0.00491 m2
      Électrik: 	0.00408 m2

