

import computations

GIRAFFE_MIN_SURFACE = 4.8   # m2
GIRAFFE_MAX_SURFACE = 9.8   # m2
STEP_SURFACE = 0.5

print(f'For a giraffe with a surface ranging from {GIRAFFE_MIN_SURFACE} to {GIRAFFE_MAX_SURFACE}, we need…')

print(f'surface\tnb_kiri\tcost')
for surface10 in range(round(GIRAFFE_MIN_SURFACE*10), round(GIRAFFE_MAX_SURFACE*10)+1, round(STEP_SURFACE*10)):
    surface = surface10 / 10
    nb_kiri, cost = computations.compute_kiri(surface)
    print(f'{surface}\t{nb_kiri}\t{cost}')



