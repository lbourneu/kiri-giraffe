"""Definition of computations routines"""

import math


ONE_KIRI_SURFACE = 0.016   # m2
GIRAFFE_MEAN_COST = 1500   # €
KIRI_COST_24 = 4.11  # €
KIRI_COST_12 = 2.47  # €
KIRI_COST_1 = 2.47/12  # €


def compute_surface(mass, size) -> float:
    """
    mass -- kg
    size -- meter
    returns the surface in m2 using the Boyd equation
    """
    return 0.0003207 * mass ** (0.7285-0.0188 * math.log(mass, 10)) * size ** 0.3
    # other equation:
    return 0.1667 * (mass ** 0.5) * (size ** 0.5)


def compute_kiri(surface_to_cover:float):
    """Computation of the cost and quantity of Kiri needed to cover a given surface.

    """
    nb_kiri = math.ceil(surface_to_cover / ONE_KIRI_SURFACE)
    cost = sum((
        KIRI_COST_24 * nb_kiri // 24,
        KIRI_COST_12 * (nb_kiri % 24) // 12,
        KIRI_COST_1  * ((nb_kiri % 24) % 12),
    ))
    return nb_kiri, round(cost, 2)
