"""Extract meaningful data from the pokdata.csv file"""

import csv
import computations
from itertools import starmap
from collections import defaultdict

DATAFILE = 'pokdata.csv'
CATEGORY_EGGROUP = 'Catégorie:Pokémon du groupe '
GENERATION_NAMES = {
    'Première': 1,
    'Deuxième': 2,
    'Troisième': 3,
    'Quatrième': 4,
    'Cinquième': 5,
    'Sixième': 6,
    'Septième': 7,
    'Huitième': 8,
}

def extract_mass_and_size(value:str) -> float:
    """May be a float written with comma, or a list of mini;normal;maxi;ultra or other size. We use the middle-leftmost
    See https://www.pokepedia.fr/Banshitrouye for instance.
    or, more complex, https://www.pokepedia.fr/Bor%C3%A9as
    """
    if ';' in value:
        value = value.split(';')
        index = max(len(value) // 2 - 1, 0)  # leftmost middle
        value = value[index]  # return the 'normal' size
    if value:
        value = value.replace(',', '.').strip('"')
    else:
        value = 0
    return float(value)

class Pokemon:
    def __init__(self, name, num, mass, size, type1, type2, categories, generation):
        self.name = name.strip('"')
        self.num = int(num.strip('"'))
        self.mass = extract_mass_and_size(mass)
        self.size = extract_mass_and_size(size)
        self.surface = computations.compute_surface(self.mass, self.size)
        self.types = {t for types in {type1.strip('"'), type2.strip('"')} for t in types.split(';') if t}
        categories = categories.split(';')
        self.egg_categories = {
            cat[len(CATEGORY_EGGROUP):]
            for cat in categories
            if cat.startswith(CATEGORY_EGGROUP)
        }
        self.gen = GENERATION_NAMES[generation.split(' ')[0]]

    def __str__(self):
        return f'<{self.name} #{self.num} of type {"&".join(self.types)} and egg space {"&".join(self.egg_categories)}>'

    def __repr__(self):
        return f'<{self.name} #{self.num} of type {"&".join(self.types)}>'


def get_pokemons() -> [list]:
    with open(DATAFILE) as fd:
        reader = csv.reader(fd)
        next(reader)  # pass header
        yield from starmap(Pokemon, reader)


def ensure_all_pokemons_are_here(pokemons:tuple=None) -> dict:
    "highlight unexpected data"
    print('Searching for discrepencies…')
    pokemons = pokemons if pokemons else tuple(get_pokemons())
    pokemons = tuple(sorted(pokemons, key=lambda p: p.num))
    error_offset = 0
    for expected_num, pokemon in enumerate(pokemons, start=1):
        if (expected_num+error_offset) != pokemon.num:
            print(pokemon)
            input(rf'/!\ expected: {expected_num+error_offset}   found:{pokemon.num}')
            error_offset -= 1
    print('Done!')

def get_pokemons_by_type(pokemons:tuple=None) -> dict:
    "return {type: set of pokemons}"
    pokemons = pokemons if pokemons else tuple(get_pokemons())
    by_types = defaultdict(set)
    for pokemon in pokemons:
        for t in pokemon.types:
            by_types[t].add(pokemon)
    return dict(by_types)

if __name__ == "__main__":
    pokemons = tuple(get_pokemons())
    ensure_all_pokemons_are_here(pokemons)
    pokemons_by_type = get_pokemons_by_type(pokemons)
    types = sorted(tuple(pokemons_by_type.keys()))
    for type in types:
        surfaces = {p.surface for p in pokemons if type in p.types}
        mean_surface = sum(surfaces) / len(surfaces)
        print(f'{type.rjust(10)}: \t{round(mean_surface, 5)} m2')
