"""Modify in-place the given pokémon CSV datafile got from pokepedia.

usage:
    python data_encoding.py <csv file>

URL:
    https://www.pokepedia.fr/index.php?title=Sp%C3%A9cial:Requ%C3%AAter&#search

Conditions:
    [[Catégorie:Page de Pokémon]] [[Numéro National::+]]

Selections:
    ?Numéro National
    ?Poids
    ?Taille
    ?Premier type
    ?Second type
    ?Catégorie
    ?Génération du Pokémon

"""

import sys
import csv

# pokemon with following names ending with this are doublons specific to some version
SPECIAL_POKEMON_SUFFIXES = (' Gigamax', " d'Alola", ' de Galar', " Ailes de l'Aurore", ' Crinière du Couchant', ' Blanc', ' Noir')

def make_new_records(fname:str) -> [str]:
    with open(fname) as fd:
        reader = csv.reader(fd)
        header = next(reader)
        header[0] = 'Pokémon'
        yield header  # fix the name of first column
        for record in reader:
            name = record[0]
            if name.endswith(SPECIAL_POKEMON_SUFFIXES):  # ignore these
                continue
            if name.startswith('Ultra-'):  # ignore these
                continue
            yield record


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print(__doc__)
        exit(1)
    records = tuple(make_new_records(sys.argv[1]))
    with open(sys.argv[1], 'w') as fd:
        writer = csv.writer(fd)
        for record in records:
            writer.writerow(record)
